package it.rome.pang.stage;

import it.rome.game.attribute.Bound;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.screen.AbstractBaseStage;
import it.rome.game.system.RenderableSystem;
import it.rome.game.system.SpriteAnimationSystem;
import it.rome.game.system.SpritePositionSystem;
import it.rome.game.system.render.Layer;
import it.rome.game.system.render.RenderableLayer;
import it.rome.pang.component.BallComponent.Size;
import it.rome.pang.factory.GameObjectFactory;
import it.rome.pang.system.BallsMovementSystem;
import it.rome.pang.system.CollisionSystem;
import it.rome.pang.system.MovementSystem;
import it.rome.pang.system.PlayerControllerSystem;
import it.rome.pang.system.RopePositionSystem;
import it.rome.pang.system.UISystem;

import com.artemis.World;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class PlayStage extends AbstractBaseStage {

  private OrthographicCamera camera;
  private OrthographicCamera uiCamera;

  private RenderableLayer renderableLayer;
  private Bound globalBound;


  public PlayStage(World world, OrthographicCamera camera, OrthographicCamera uiCamera) {
    super(world);
    this.camera = camera;
    this.globalBound = new Bound(0, 0, camera.viewportWidth, camera.viewportHeight);
    GameObjectFactory.init(world);
    SpriteBatch spriteBatch = new SpriteBatch();
    Layer background = new Layer(1, camera, spriteBatch);
    Layer balls = new Layer(2, camera, spriteBatch);
    Layer player = new Layer(3, camera, spriteBatch);
    renderableLayer = new RenderableLayer();
    renderableLayer.add("background", background);
    renderableLayer.add("balls", balls);
    renderableLayer.add("player", player);
    initWorld();
    initGame();
  }

  private void initWorld() {
    world.setSystem(new PlayerControllerSystem());
    world.setSystem(new BallsMovementSystem(globalBound, .1f));
    world.setSystem(new MovementSystem());
    world.setSystem(new CollisionSystem());
    world.setSystem(new RopePositionSystem(globalBound));
    world.setSystem(new SpritePositionSystem());
    world.setSystem(new SpriteAnimationSystem());
    world.setSystem(new RenderableSystem(camera, renderableLayer));
    world.setSystem(new UISystem(uiCamera));
    // world.setSystem(new DebugSpatialsystem());
  }

  private void initGame() {
    initBackground();
    initBalls();
    initBlock();
  }

  private void initBackground() {
    GameObjectFactory.createBackground(new SpatialImpl(camera.viewportWidth * .5f,
        camera.viewportHeight * .5f, camera.viewportWidth, camera.viewportHeight));
  }

  private void initBalls() {
    GameObjectFactory.createBall(new SpatialImpl(0, 0, 80, 80), Size.L);
    GameObjectFactory.createBall(new SpatialImpl(150, 100, 40, 40), Size.M);
    GameObjectFactory.createBall(new SpatialImpl(150, 100, 40, 40), Size.S);
  }

  private void initBlock() {
    GameObjectFactory.createBlock(new SpatialImpl(100, 200));
    GameObjectFactory.createBlock(new SpatialImpl(500, 200));
  }

}
