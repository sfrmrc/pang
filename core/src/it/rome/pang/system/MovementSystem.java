package it.rome.pang.system;

import it.rome.game.attribute.Spatial;
import it.rome.game.component.MovementComponent;
import it.rome.game.component.SpatialComponent;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.2
 * 
 */
@Wire
public class MovementSystem extends EntityProcessingSystem {

  ComponentMapper<SpatialComponent> sm;
  ComponentMapper<MovementComponent> mm;

  @SuppressWarnings("unchecked")
  public MovementSystem() {
    super(Aspect.getAspectForAll(SpatialComponent.class, MovementComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    SpatialComponent spatialComponent = sm.get(entity);
    MovementComponent movementComponent = mm.get(entity);
    Spatial spatial = spatialComponent.getSpatial();
    float x = spatial.getX() + movementComponent.getMovement().x;
    float y = spatial.getY() + movementComponent.getMovement().y;
    spatial.setPosition(x, y);
  }
}
