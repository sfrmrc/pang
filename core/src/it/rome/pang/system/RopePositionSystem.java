package it.rome.pang.system;

import it.rome.game.attribute.Bound;
import it.rome.game.attribute.Spatial;
import it.rome.game.component.SpatialComponent;
import it.rome.pang.component.RopeComponent;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
@Wire
public class RopePositionSystem extends EntityProcessingSystem {

  ComponentMapper<RopeComponent> rm;
  ComponentMapper<SpatialComponent> sm;

  Bound globalBound;

  @SuppressWarnings("unchecked")
  public RopePositionSystem(Bound globalBound) {
    super(Aspect.getAspectForAll(RopeComponent.class, SpatialComponent.class));
    this.globalBound = globalBound;
  }

  @Override
  protected void process(Entity entity) {
    SpatialComponent spatialComponent = sm.get(entity);
    RopeComponent ropeComponent = rm.get(entity);
    Spatial spatial = spatialComponent.getSpatial();
    float h = spatial.getHeight() + ropeComponent.getSpeed();
    if (h > globalBound.getHiHeight()) {
      entity.deleteFromWorld();
      return;
    }
    spatial.setSize(spatial.getWidth(), h);
    float y = spatial.getY() + ropeComponent.getSpeed() * .5f;
    spatial.setPosition(spatial.getX(), y);
  }
}
