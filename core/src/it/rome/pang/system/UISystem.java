package it.rome.pang.system;

import it.rome.game.attribute.EntityRef;
import it.rome.pang.Tags;
import it.rome.pang.component.LifeComponent;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
@Wire
public class UISystem extends VoidEntitySystem {

  private static final BitmapFont font = new BitmapFont();
  private static final SpriteBatch spriteBatch = new SpriteBatch();
  private final Texture avatar;

  ComponentMapper<LifeComponent> lm;

  OrthographicCamera camera;
  EntityRef player;

  public UISystem(OrthographicCamera camera) {
    this.camera = camera;
    this.avatar = new Texture("avatar.png");
  }

  @Override
  protected void begin() {
    super.begin();
    this.player = new EntityRef(world.getManager(TagManager.class).getEntity(Tags.Player));
  }

  @Override
  protected void processSystem() {
    int life = lm.get(player.getEntity()).getLife();
    spriteBatch.begin();
    font.draw(spriteBatch, "Life", 20, 35);
    for (int i = 1; i <= life; i++) {
      spriteBatch.draw(avatar, 40 + 20 * i, 20);
    }
    spriteBatch.end();
  }

}
