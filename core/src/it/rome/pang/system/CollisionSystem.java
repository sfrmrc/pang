package it.rome.pang.system;

import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.CasterComponent;
import it.rome.game.component.MovementComponent;
import it.rome.game.component.ScoreComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.TagComponent;
import it.rome.pang.Tags;
import it.rome.pang.component.BallComponent;
import it.rome.pang.component.BallComponent.Size;
import it.rome.pang.component.LifeComponent;
import it.rome.pang.component.RopeComponent;
import it.rome.pang.component.StateComponent;
import it.rome.pang.factory.GameObjectFactory;
import it.rome.pang.template.GameObjectTemplate.Direction;
import it.rome.pang.template.PlayerState;

import java.util.ArrayList;
import java.util.Collection;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Manages collisions between {@link Tags#Ball}, {@link Tags#Rope} and {@link Tags#Block}
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
@Wire
public class CollisionSystem extends VoidEntitySystem {

  TagManager tagManager;
  GroupManager groupManager;

  ComponentMapper<LifeComponent> lm;
  ComponentMapper<BallComponent> bm;
  ComponentMapper<SpatialComponent> sm;
  ComponentMapper<TagComponent> tm;
  ComponentMapper<MovementComponent> mm;
  ComponentMapper<RopeComponent> rm;
  ComponentMapper<CasterComponent> cm;
  ComponentMapper<ScoreComponent> scm;
  ComponentMapper<StateComponent> stm;

  Entity player;
  Spatial playerSpatial;

  @Override
  protected void processSystem() {
    player = tagManager.getEntity(Tags.Player);
    playerSpatial = sm.get(player).getSpatial();
    ImmutableBag<Entity> balls = groupManager.getEntities(Tags.Ball);
    ImmutableBag<Entity> ropes = groupManager.getEntities(Tags.Rope);
    ImmutableBag<Entity> blocks = groupManager.getEntities(Tags.Block);
    Collection<Entity> collisions = new ArrayList<Entity>();
    for (Entity ball : balls) {
      Spatial ballSpatial = sm.get(ball).getSpatial();
      if (checkCircleRectangleCollision(ballSpatial, playerSpatial)) {
        StateComponent stateComponent = stm.get(player);
        stateComponent.change(PlayerState.DEAD);
        lm.get(player).increment(-1);
        break;
      }
      for (Entity rope : ropes) {
        Spatial rectangleSpatial = sm.get(rope).getSpatial();
        if (checkCircleRectangleCollision(ballSpatial, rectangleSpatial)) {
          rope.deleteFromWorld();
          collisions.add(ball);
          break;
        }
      }
      MovementComponent movementComponent = mm.get(ball);
      Vector2 speed = movementComponent.getMovement();
      Vector2 center = new Vector2(ballSpatial.getX(), ballSpatial.getY());
      float area = ballSpatial.getWidth() * ballSpatial.getWidth() * .25f;
      for (Entity block : blocks) {
        Spatial blockSpatial = sm.get(block).getSpatial();
        checkBlockCollision(ballSpatial, center, area, speed, blockSpatial);
      }
    }
    for (Entity rope : ropes) {
      RopeComponent ropeComponent = rm.get(rope);
      Spatial ropeSpatial = sm.get(rope).getSpatial();
      float ropeOffsetX = ropeSpatial.getX() - ropeSpatial.getWidth() * .5f;
      float ropeOffsetY =
          ropeSpatial.getY() - ropeSpatial.getHeight() * .5f + ropeComponent.getSpeed();
      Rectangle ropeRectangle =
          new Rectangle(ropeOffsetX, ropeOffsetY, ropeSpatial.getWidth(), ropeSpatial.getHeight());
      for (Entity block : blocks) {
        Spatial blockSpatial = sm.get(block).getSpatial();
        float blockOffsetX = blockSpatial.getX() - blockSpatial.getWidth() * .5f;
        float blockOffsetY = blockSpatial.getY() - blockSpatial.getHeight() * .5f;
        Rectangle blockRectangle =
            new Rectangle(blockOffsetX, blockOffsetY, blockSpatial.getWidth(),
                blockSpatial.getHeight());
        if (Intersector.intersectRectangles(ropeRectangle, blockRectangle, new Rectangle())) {
          CasterComponent casterComponent = cm.get(rope);
          ScoreComponent scoreComponent = scm.get(casterComponent.getEntityRef().getEntity());
          scoreComponent.increment(1);
          rope.deleteFromWorld();
          break;
        }
      }
    }
    for (Entity ball : collisions) {
      SpatialComponent ballSpatial = sm.get(ball);
      handleBallExplosion(ball, ballSpatial.getSpatial());
    }
  }

  private void handleBallExplosion(Entity ball, Spatial ballSpatial) {
    Size newSize = Size.values()[bm.get(ball).getSize().ordinal() + 1];
    if (newSize != Size.X) {
      float radius = ballSpatial.getWidth() * .5f;
      Spatial spatial = new SpatialImpl(ballSpatial.getX() - radius, ballSpatial.getY());
      GameObjectFactory.createBall(spatial, newSize, Direction.LEFT);
      spatial = new SpatialImpl(ballSpatial.getX() + radius, ballSpatial.getY());
      GameObjectFactory.createBall(spatial, newSize, Direction.RIGHT);
    }
    GameObjectFactory.createText("BOOM!", ballSpatial, Color.RED);
    ball.deleteFromWorld();
  }

  // Ball vs Block
  private void checkBlockCollision(Spatial spatial, Vector2 center, float area, Vector2 speed,
      Spatial blockSpatial) {
    float halfWidth = blockSpatial.getWidth() * .5f;
    float halfHeight = blockSpatial.getHeight() * .5f;
    Vector2 x1 = new Vector2(blockSpatial.getX() - halfWidth, blockSpatial.getY() - halfHeight);
    Vector2 x2 = new Vector2(blockSpatial.getX() - halfWidth, blockSpatial.getY() + halfHeight);
    Vector2 x3 = new Vector2(blockSpatial.getX() + halfWidth, blockSpatial.getY() + halfHeight);
    Vector2 x4 = new Vector2(blockSpatial.getX() + halfWidth, blockSpatial.getY() - halfHeight);
    Vector2 projection = new Vector2(center.x + speed.x, center.y + speed.y);
    if (Intersector.intersectSegmentCircle(x1, x2, projection, area) && projection.x < x1.x) {
      // left
      speed.x = -Math.abs(speed.x);
    }
    if (Intersector.intersectSegmentCircle(x3, x4, projection, area) && projection.x > x3.x) {
      // right
      speed.x = Math.abs(speed.x);
    }
    if (Intersector.intersectSegmentCircle(x2, x3, projection, area) && projection.y > x2.y) {
      // top
      speed.y = Math.abs(speed.y);
    }
    if (Intersector.intersectSegmentCircle(x4, x1, projection, area) && projection.y <= x2.y) {
      // bottom
      speed.y = -Math.abs(speed.y);
    }
  }

  // Ball vs Rope
  private boolean checkCircleRectangleCollision(Spatial circle, Spatial rectangle) {
    float rxi = rectangle.getX() - rectangle.getWidth() * .5f;
    float rxf = rectangle.getX() + rectangle.getWidth() * .5f;
    float radius = circle.getWidth() * .5f;
    if (rxi > circle.getX() + radius) {
      return false;
    }
    if (rxf < circle.getX() - radius) {
      return false;
    }
    float ryi = rectangle.getY() - rectangle.getHeight() * .5f;
    float ryf = rectangle.getY() + rectangle.getHeight() * .5f;
    if (ryi > circle.getY() + radius) {
      return false;
    }
    if (ryf < circle.getY() - radius) {
      return false;
    }
    return true;
  }

}
