package it.rome.pang.system;

import it.rome.game.attribute.Bound;
import it.rome.game.attribute.Spatial;
import it.rome.game.component.BoundComponent;
import it.rome.game.component.MovementComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.pang.component.BallComponent;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.GroupManager;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.math.Vector2;

/**
 * Manages {@link BallComponent}
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
@Wire
public class BallsMovementSystem extends EntityProcessingSystem {

  Bound globalBound;
  float gravity;

  GroupManager groupManager;

  ComponentMapper<BallComponent> blm;
  ComponentMapper<MovementComponent> mm;
  ComponentMapper<BoundComponent> bm;
  ComponentMapper<SpatialComponent> sm;

  @SuppressWarnings("unchecked")
  public BallsMovementSystem(Bound globalBound, float gravity) {
    super(Aspect.getAspectForAll(BallComponent.class, MovementComponent.class,
        SpatialComponent.class));
    this.globalBound = globalBound;
    this.gravity = gravity;
  }

  @Override
  protected void process(Entity entity) {
    BallComponent ballComponent = blm.get(entity);
    SpatialComponent spatialComponent = sm.get(entity);
    MovementComponent movementComponent = mm.get(entity);
    Spatial spatial = spatialComponent.getSpatial();
    Vector2 speed = movementComponent.getMovement();
    speed.y -= gravity;
    bounce(globalBound, spatial, speed, ballComponent.getMaxHeight());
  }

  private void bounce(Bound bound, Spatial spatial, Vector2 speed, float maxVelocity) {
    float radius = spatial.getHeight() * .5f;
    if (spatial.getY() > bound.getHiHeight() - radius) {
      speed.y = -maxVelocity;
    }
    if (spatial.getY() < bound.getLowHeight() + radius) {
      speed.y = maxVelocity;
    }
    if (spatial.getX() < bound.getLowWidth() + radius) {
      speed.x = Math.abs(speed.x);
    }
    if (spatial.getX() > bound.getHiWidth() - radius) {
      speed.x = -Math.abs(speed.x);
    }
  }

}
