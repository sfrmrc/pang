package it.rome.pang.system;

import it.rome.game.attribute.EntityRef;
import it.rome.pang.component.StateComponent;
import it.rome.pang.component.WeaponComponent;
import it.rome.pang.factory.GameObjectFactory;
import it.rome.pang.template.PlayerState;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
@Wire
public class PlayerControllerSystem extends EntityProcessingSystem {

  ComponentMapper<WeaponComponent> wm;
  ComponentMapper<StateComponent> im;

  @SuppressWarnings("unchecked")
  public PlayerControllerSystem() {
    super(Aspect.getAspectForAll(StateComponent.class, WeaponComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    StateComponent inputComponent = im.get(entity);
    inputComponent.change(PlayerState.VOID);
    if (Gdx.input.isKeyPressed(Keys.LEFT)) {
      inputComponent.change(PlayerState.LEFT);
    }
    if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
      inputComponent.change(PlayerState.RIGHT);
    }
    if (wm.has(entity)) {
      WeaponComponent weaponComponent = wm.get(entity);
      weaponComponent.decrement(world.delta);
      if (Gdx.input.isKeyPressed(Keys.SPACE) && weaponComponent.isAvaiable()) {
        weaponComponent.start();
        GameObjectFactory.createRope(new EntityRef(entity));
      }
    }
  }
}
