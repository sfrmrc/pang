package it.rome.pang;

import it.rome.pang.screen.MainMenuScreen;

import com.badlogic.gdx.Game;

public class Pang extends Game {

  @Override
  public void create() {
    setScreen(new MainMenuScreen(this));
  }

}
