package it.rome.pang.screen;

import it.rome.game.screen.AbstractBaseScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * Shows about stage
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class AboutScreen extends AbstractBaseScreen {

  private Texture fontTexture = new Texture(Gdx.files.internal("fonts/normal_0.png"));

  private Stage stage;
  private Label backButton;

  public AboutScreen(final Game game) {
    super(game);
    stage = new Stage();
    float halfWidth = stage.getWidth() * .5f;
    float halfHeight = stage.getHeight() * .5f;
    TextureRegion fontRegion = new TextureRegion(fontTexture);
    BitmapFont bitmapFont =
        new BitmapFont(Gdx.files.internal("fonts/normal.fnt"), fontRegion, false);
    LabelStyle oldStyle = new LabelStyle(bitmapFont, Color.WHITE);
    backButton = new Label("Back", oldStyle);
    backButton.setTouchable(Touchable.enabled);
    backButton.setBounds(0, 0, backButton.getWidth(), backButton.getHeight());
    backButton.addListener(new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        game.setScreen(new MainMenuScreen(game));
        return true;
      }
    });
    Label name = new Label("Name: Marco", oldStyle);
    name.setPosition(halfWidth - name.getWidth(), halfHeight + name.getHeight());
    Label secondname = new Label("Second Name: Sferra", oldStyle);
    secondname.setPosition(halfWidth - name.getWidth(), halfHeight);
    Label link = new Label("linkedin: http://it.linkedin.com/in/marcosferra/", oldStyle);
    link.setPosition(halfWidth - name.getWidth(), halfHeight - link.getHeight());
    link.setFontScale(.7f);
    stage.addActor(backButton);
    stage.addActor(name);
    stage.addActor(secondname);
    stage.addActor(link);
    Gdx.input.setInputProcessor(stage);
  }

  @Override
  public void render(float delta) {
    super.render(delta);
    stage.act(delta);
    stage.draw();
  }

  @Override
  public void hide() {
    stage.dispose();
  }

}
