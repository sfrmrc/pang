package it.rome.pang.screen;

import it.rome.game.attribute.Bound;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.screen.AbstractBaseScreen;
import it.rome.pang.Tags;
import it.rome.pang.component.LifeComponent;
import it.rome.pang.component.StateComponent;
import it.rome.pang.factory.GameObjectFactory;
import it.rome.pang.stage.PlayStage;
import it.rome.pang.template.PlayerState;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class GameScreen extends AbstractBaseScreen {

  OrthographicCamera playCamera;
  OrthographicCamera uiCamera;

  Stage view;
  TagManager tagManager;
  GroupManager groupManager;

  Entity player;
  StateComponent stateComponent;

  public GameScreen(Game game) {
    super(game);
    float w = Gdx.graphics.getWidth();
    float h = Gdx.graphics.getHeight();
    uiCamera = new OrthographicCamera(w, h);
    playCamera = new OrthographicCamera(w, 300);
    playCamera.setToOrtho(false, w, h);
    uiCamera.setToOrtho(false, w, h);
    playCamera.zoom = 1.1f;
    playCamera.translate(0, -30);
    uiCamera.update();
    playCamera.update();
    initWorld(3);
  }

  private void initWorld(int life) {
    world = new World();
    tagManager = new TagManager();
    groupManager = new GroupManager();
    world.setManager(tagManager);
    world.setManager(groupManager);
    world.initialize();
    view = new PlayStage(world, playCamera, uiCamera);
    initPlayer(life);
  }

  @Override
  public void render(float delta) {
    super.render(delta);
    world.setDelta(delta);
    world.process();
    view.act(delta);
    view.draw();
    player = tagManager.getEntity(Tags.Player);
    stateComponent = player.getComponent(StateComponent.class);
    ImmutableBag<Entity> balls = groupManager.getEntities(Tags.Ball);
    if (stateComponent.current() == PlayerState.DEAD || balls.size() == 0) {
      LifeComponent lifeComponent = player.getComponent(LifeComponent.class);
      if (lifeComponent.getLife() == 0) {
        gameOver();
      } else {
        world.dispose();
        initWorld(lifeComponent.getLife());
      }
    }
  }

  private void gameOver() {
    game.setScreen(new MainMenuScreen(game));
  }

  @Override
  public void hide() {
    super.dispose();
    view.dispose();
    world.dispose();
  }

  private void initPlayer(int life) {
    GameObjectFactory.createPlayer(new SpatialImpl(playCamera.viewportWidth * .5f, 30f, 40, 40),
        new Bound(0, 0, playCamera.viewportWidth, playCamera.viewportHeight), life);
  }

}
