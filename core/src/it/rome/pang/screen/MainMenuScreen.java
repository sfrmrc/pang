package it.rome.pang.screen;

import it.rome.game.screen.AbstractBaseScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class MainMenuScreen extends AbstractBaseScreen {

  private Texture fontTexture = new Texture(Gdx.files.internal("fonts/normal_0.png"));

  private Stage stage;
  private Label startButton;
  private Label aboutButton;

  public MainMenuScreen(final Game game) {
    super(game);
    stage = new Stage();
    TextureRegion fontRegion = new TextureRegion(fontTexture);
    BitmapFont bitmapFont =
        new BitmapFont(Gdx.files.internal("fonts/normal.fnt"), fontRegion, false);

    LabelStyle style = new LabelStyle(bitmapFont, Color.WHITE);
    startButton = new Label("Start", style);
    startButton.setTouchable(Touchable.enabled);
    float halfWidth = stage.getWidth() * .5f;
    float halfHeight = stage.getHeight() * .5f;
    startButton.setBounds(halfWidth - 30, halfHeight - 150, startButton.getWidth(),
        startButton.getHeight());
    startButton.addListener(new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        game.setScreen(new GameScreen(game));
        return true;
      }
    });
    aboutButton = new Label("About", style);
    aboutButton.setTouchable(Touchable.enabled);
    aboutButton.setBounds(halfWidth - 30, halfHeight - 200, aboutButton.getWidth(),
        aboutButton.getHeight());
    aboutButton.addListener(new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        game.setScreen(new AboutScreen(game));
        return true;
      }
    });
    Label commands = new Label("Move: <- -> Fire: SPACE", style);
    Image splash = new Image(new Texture("pang.jpg"));
    stage.addActor(splash);
    stage.addActor(startButton);
    stage.addActor(aboutButton);
    stage.addActor(commands);
    Gdx.input.setInputProcessor(stage);
  }

  @Override
  public void render(float delta) {
    super.render(delta);
    stage.act(delta);
    stage.draw();
  }

  @Override
  public void hide() {
    stage.dispose();
  }

}
