package it.rome.pang.factory;

import it.rome.game.attribute.Bound;
import it.rome.game.attribute.EntityRef;
import it.rome.game.attribute.Spatial;
import it.rome.game.template.EntityFactory;
import it.rome.game.template.Parameters;
import it.rome.game.template.TextTemplate;
import it.rome.pang.component.BallComponent.Size;
import it.rome.pang.template.BackgroundTemplate;
import it.rome.pang.template.GameObjectTemplate;
import it.rome.pang.template.GameObjectTemplate.Direction;
import it.rome.pang.template.GameObjectTemplate.Type;
import it.rome.pang.template.PlayerTemplate;
import it.rome.pang.template.RopeTemplate;

import com.artemis.World;
import com.badlogic.gdx.graphics.Color;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class GameObjectFactory {

  private static EntityFactory entityFactory;
  private static final BackgroundTemplate backgroundTemplate = new BackgroundTemplate();
  private static final GameObjectTemplate gameObjectTemplate = new GameObjectTemplate();
  private static final PlayerTemplate playerTemplate = new PlayerTemplate();
  private static final RopeTemplate ropeTemplate = new RopeTemplate();
  private static final TextTemplate textTemplate = new TextTemplate();

  public static void init(World world) {
    entityFactory = new EntityFactory(world);
  }

  public static void createPlayer(Spatial spatial, Bound bound, int life) {
    entityFactory.instantiate(playerTemplate,
        new Parameters().put("bound", bound).put("spatial", spatial).put("life", life));
  }

  public static void createBackground(Spatial spatial) {
    entityFactory.instantiate(backgroundTemplate, new Parameters().put("spatial", spatial));
  }

  public static void createBall(Spatial spatial, Size size) {
    entityFactory.instantiate(gameObjectTemplate,
        new Parameters().put("size", size).put("type", Type.BALL).put("spatial", spatial));
  }

  public static void createBall(Spatial spatial, Size size, Direction direction) {
    entityFactory.instantiate(
        gameObjectTemplate,
        new Parameters().put("size", size).put("type", Type.BALL).put("spatial", spatial)
            .put("direction", direction));
  }

  public static void createBlock(Spatial spatial) {
    entityFactory.instantiate(gameObjectTemplate,
        new Parameters().put("type", Type.BLOCK).put("spatial", spatial));
  }

  public static void createRope(EntityRef entityRef) {
    entityFactory.instantiate(ropeTemplate, new Parameters().put("caster", entityRef));
  }

  public static void createText(String message, Spatial spatial, Color color) {
    entityFactory.instantiate(
        textTemplate,
        new Parameters().put("spatial", spatial).put("text", message).put("color", color)
            .put("fading", 2f).put("layer", 1));
  }

  public static void createWall(Spatial spatial) {
    entityFactory.instantiate(gameObjectTemplate,
        new Parameters().put("type", Type.WALL).put("spatial", spatial));
  }

}
