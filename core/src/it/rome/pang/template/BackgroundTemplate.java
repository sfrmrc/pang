package it.rome.pang.template;

import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.template.EntityTemplate;
import it.rome.pang.factory.GameObjectFactory;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class BackgroundTemplate extends EntityTemplate {

  Texture texture = new Texture("background.png");

  public BackgroundTemplate() {}

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    Spatial spatial = parameters.get("spatial");

    if (spatial == null) {
      throw new IllegalArgumentException("Spatial parameter cannot be null");
    }

    Sprite sprite = new Sprite(texture);

    SpriteComponent spriteComponent = new SpriteComponent(sprite);
    SpatialComponent spatialComponent = new SpatialComponent(spatial);

    entityEdit.add(spriteComponent);
    entityEdit.add(spatialComponent);
    entityEdit.add(new RenderableComponent(1));

    int floor = new Float(spatial.getWidth() / 25).intValue() + 1;
    float x = 0f;
    for (int i = 0; i < floor; i++) {
      GameObjectFactory.createWall(new SpatialImpl(x, 0f));
      x += 25f;
    }

  }
}
