package it.rome.pang.template;

import it.rome.game.attribute.EntityRef;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.CasterComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteAnimationComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.component.TagComponent;
import it.rome.game.template.EntityTemplate;
import it.rome.pang.Tags;
import it.rome.pang.component.RopeComponent;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.managers.GroupManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Creates the rope entity
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class RopeTemplate extends EntityTemplate {

  private Texture texture;

  public RopeTemplate() {
    texture = new Texture("rope.png");
  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    EntityRef entityCaster = parameters.get("caster");

    if (entityCaster == null) {
      throw new IllegalArgumentException("Caster parameter cannot be null");
    }

    Spatial spatialCaster =
        entityCaster.getEntity().getComponent(SpatialComponent.class).getSpatial();
    SpatialImpl spatial =
        new SpatialImpl(spatialCaster.getX(), spatialCaster.getY(), 20, spatialCaster.getHeight());

    SpriteComponent spriteComponent = new SpriteComponent(new Sprite(texture));
    SpriteAnimationComponent spriteAnimationComponent =
        new SpriteAnimationComponent(texture, 0, .3f, PlayMode.LOOP, 3, 1);

    SpatialComponent spatialComponent = new SpatialComponent(new SpatialImpl(spatial));

    entityEdit.add(new TagComponent(Tags.Rope));
    entityEdit.add(new RopeComponent(20f, 4f, texture));
    entityEdit.add(new CasterComponent(entityCaster));
    entityEdit.add(spriteAnimationComponent);
    entityEdit.add(spriteComponent);
    entityEdit.add(spatialComponent);
    entityEdit.add(new RenderableComponent(1));

    entity.getWorld().getManager(GroupManager.class).add(entity, Tags.Rope);

  }

}
