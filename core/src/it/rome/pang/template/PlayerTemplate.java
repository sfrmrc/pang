package it.rome.pang.template;

import it.rome.game.attribute.Bound;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.BoundComponent;
import it.rome.game.component.MovementComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.ScoreComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteAnimationComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.component.TagComponent;
import it.rome.game.template.EntityTemplate;
import it.rome.pang.Tags;
import it.rome.pang.component.LifeComponent;
import it.rome.pang.component.StateComponent;
import it.rome.pang.component.WeaponComponent;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.fsm.EntityState;
import com.artemis.fsm.EntityStateMachine;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class PlayerTemplate extends EntityTemplate {

  private final Texture texture;

  public PlayerTemplate() {
    this.texture = new Texture("player.png");
  }


  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    Spatial spatial = parameters.get("spatial");
    Bound bound = parameters.get("bound");
    Integer life = parameters.get("life");

    if (spatial == null) {
      throw new IllegalArgumentException("Spatial parameter cannot be null");
    }

    Sprite sprite = new Sprite(texture);

    SpatialComponent spatialComponent = new SpatialComponent(new SpatialImpl(spatial));
    SpriteComponent spriteComponent = new SpriteComponent(sprite);

    EntityStateMachine<PlayerState> esm = new EntityStateMachine<PlayerState>(entity);
    createVoidState(esm, entity);
    createLeftState(esm, entity);
    createRightState(esm, entity);
    createDeadState(esm, entity);

    if (bound != null) {
      entityEdit.add(new BoundComponent(bound));
    }

    entityEdit.add(new LifeComponent(life));
    entityEdit.add(new TagComponent(Tags.Player));
    entityEdit.add(new RenderableComponent(1));
    entityEdit.add(spriteComponent);
    entityEdit.add(spatialComponent);
    entityEdit.add(new WeaponComponent(.5f));
    entityEdit.add(new StateComponent(esm));
    entityEdit.add(new ScoreComponent(0));

    esm.changeState(PlayerState.VOID);

    entity.getWorld().getManager(TagManager.class).register(Tags.Player, entity);

  }

  private void createRightState(EntityStateMachine<PlayerState> esm, Entity entity) {
    EntityEdit entityEdit = entity.edit();

    SpriteAnimationComponent spriteAnimationComponent =
        new SpriteAnimationComponent(texture, 0, 0.2f, PlayMode.NORMAL, 22, 1, 0, 5);
    entityEdit.add(spriteAnimationComponent);

    EntityState<PlayerState> state = esm.createState(PlayerState.RIGHT);
    state.add(SpriteAnimationComponent.class).withInstance(spriteAnimationComponent);
    state.add(MovementComponent.class).withInstance(new MovementComponent(2f, 0));

  }

  private void createLeftState(EntityStateMachine<PlayerState> esm, Entity entity) {
    EntityEdit entityEdit = entity.edit();

    SpriteAnimationComponent spriteAnimationComponent =
        new SpriteAnimationComponent(texture, 0, 0.2f, PlayMode.NORMAL, 22, 1, 12, 5);
    entityEdit.add(spriteAnimationComponent);

    EntityState<PlayerState> state = esm.createState(PlayerState.LEFT);
    state.add(SpriteAnimationComponent.class).withInstance(spriteAnimationComponent);
    state.add(MovementComponent.class).withInstance(new MovementComponent(-2f, 0));

  }

  private void createVoidState(EntityStateMachine<PlayerState> esm, Entity entity) {
    EntityEdit entityEdit = entity.edit();

    SpriteAnimationComponent spriteAnimationComponent =
        new SpriteAnimationComponent(texture, 0, 0.2f, PlayMode.NORMAL, 22, 1, 6, 1);
    entityEdit.add(spriteAnimationComponent);

    esm.createState(PlayerState.VOID).add(SpriteAnimationComponent.class)
        .withInstance(spriteAnimationComponent);

  }

  private void createDeadState(EntityStateMachine<PlayerState> esm, Entity entity) {
    esm.createState(PlayerState.DEAD);
  }

}
