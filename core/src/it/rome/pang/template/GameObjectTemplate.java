package it.rome.pang.template;

import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.MovementComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.component.TagComponent;
import it.rome.game.template.EntityTemplate;
import it.rome.pang.Tags;
import it.rome.pang.component.BallComponent;
import it.rome.pang.component.BallComponent.Size;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.managers.GroupManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class GameObjectTemplate extends EntityTemplate {

  public enum Type {
    BALL, BLOCK, WALL
  }

  public enum Direction {
    LEFT, RIGHT
  }

  private static TextureAtlas textureAtlas = new TextureAtlas(
      Gdx.files.internal("atlas/red_baballs.txt"));
  private static AtlasRegion ball;
  private static AtlasRegion block;
  private static AtlasRegion wall;

  static {
    ball = textureAtlas.findRegion("red_ball");
    block = textureAtlas.findRegion("block");
    wall = textureAtlas.findRegion("wall");
  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    Spatial spatial = parameters.get("spatial");
    Type type = parameters.get("type");

    if (type == null) {
      throw new IllegalArgumentException("Type parameter cannot be null");
    }

    if (type == Type.BALL) {

      Size size = parameters.get("size");

      if (size == null) {
        throw new IllegalArgumentException("Size parameter cannot be null");
      }

      Direction direction = parameters.get("direction", Direction.RIGHT);

      float d = -1;

      switch (direction) {
        case RIGHT:
          d = 1;
          break;
        case LEFT:
          d = -1;
          break;
      }

      Vector2 movement = null;
      float maxHeight = 0f;

      switch (size) {
        case L:
          maxHeight = 8.7f;
          movement = new Vector2(1.5f * d, -8f);
          spatial.setSize(90, 90);
          break;
        case M:
          maxHeight = 6.7f;
          spatial.setSize(60, 60);
          movement = new Vector2(1.5f * d, -7f);
          break;
        case S:
          maxHeight = 4f;
          spatial.setSize(17, 17);
          movement = new Vector2(1.5f * d, -5f);
          break;
        case X:
          throw new IllegalArgumentException("Type parameter cannot be X");
      }

      if (spatial == null) {
        throw new IllegalArgumentException("Spatial parameter cannot be null");
      }

      SpriteComponent spriteComponent = new SpriteComponent(new Sprite(ball));

      entityEdit.add(new TagComponent(Tags.Ball));
      entityEdit.add(new BallComponent(size, maxHeight));
      entityEdit.add(new MovementComponent(movement));
      entityEdit.add(new RenderableComponent(1));
      entityEdit.add(new SpatialComponent(new SpatialImpl(spatial)));
      entityEdit.add(spriteComponent);

      entity.getWorld().getManager(GroupManager.class).add(entity, Tags.Ball);

    } else if (type == Type.BLOCK) {
      SpriteComponent spriteComponent = new SpriteComponent(new Sprite(block));

      entityEdit.add(new TagComponent(Tags.Block));
      entityEdit.add(new RenderableComponent(1));
      entityEdit.add(new SpatialComponent(new SpatialImpl(spatial.getX(), spatial.getY(), 96, 24)));
      entityEdit.add(spriteComponent);

      entity.getWorld().getManager(GroupManager.class).add(entity, Tags.Block);
    } else if (type == Type.WALL) {
      SpriteComponent spriteComponent = new SpriteComponent(new Sprite(wall));
      entityEdit.add(new SpatialComponent(new SpatialImpl(spatial.getX(), spatial.getY(), 25, 25)));
      entityEdit.add(new RenderableComponent(1));
      entityEdit.add(spriteComponent);
    }

  }
}
