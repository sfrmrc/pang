package it.rome.pang.template;

import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.TagComponent;
import it.rome.game.template.EntityTemplate;
import it.rome.pang.Tags;

import com.artemis.Entity;
import com.artemis.EntityEdit;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class FloorTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    Spatial spatial = parameters.get("spatial");

    if (spatial == null) {
      throw new IllegalArgumentException("Spatial parameter cannot be null");
    }

    entityEdit.add(new TagComponent(Tags.Floor));
    entityEdit.add(new RenderableComponent(1));
    entityEdit.add(new SpatialComponent(new SpatialImpl(spatial)));
  }

}
