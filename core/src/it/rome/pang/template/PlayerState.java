package it.rome.pang.template;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public enum PlayerState {
  VOID, LEFT, RIGHT, DEAD
}
