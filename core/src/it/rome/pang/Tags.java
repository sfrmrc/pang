package it.rome.pang;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class Tags {

  public static final String Floor = "floor";
  public static final String Player = "player";
  public static final String Rope = "rope";
  public static final String Ball = "ball";
  public static final String Wall = "wall";
  public static final String Block = "block";

}
