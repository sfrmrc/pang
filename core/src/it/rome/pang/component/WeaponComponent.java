package it.rome.pang.component;

import com.artemis.Component;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class WeaponComponent extends Component {

  private float tick;
  private float countdown;

  public WeaponComponent(float countdown) {
    this.countdown = countdown;
    this.tick = 0;
  }

  public void decrement(float delta) {
    if (tick > 0) {
      tick -= delta;
    }
    if (tick <= 0) {
      tick = 0;
    }
  }

  public boolean isAvaiable() {
    return tick == 0;
  }

  public void start() {
    tick = countdown;
  }

}
