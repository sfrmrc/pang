package it.rome.pang.component;

import com.artemis.Component;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class BallComponent extends Component {

  /**
   * Size Large, Medium, Small, eXplode
   * 
   * @author m.sferra
   * 
   */
  public enum Size {
    L, M, S, X
  }

  float maxHeight;
  Size size;

  public BallComponent(Size size, float maxHeight) {
    this.size = size;
    this.maxHeight = maxHeight;
  }

  public Size getSize() {
    return size;
  }

  public float getMaxHeight() {
    return maxHeight;
  }

}
