package it.rome.pang.component;

import it.rome.pang.template.PlayerState;

import com.artemis.Component;
import com.artemis.fsm.EntityStateMachine;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class StateComponent extends Component {

  EntityStateMachine<PlayerState> esm;

  public StateComponent(EntityStateMachine<PlayerState> esm) {
    this.esm = esm;
  }

  public void change(PlayerState state) {
    esm.changeState(state);
  }

  public PlayerState current() {
    return esm.currentState();
  }

}
