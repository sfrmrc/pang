package it.rome.pang.component;

import com.artemis.Component;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class LifeComponent extends Component {

  private int life;

  public LifeComponent(int life) {
    this.life = life;
  }

  public int getLife() {
    return life;
  }

  public void setLife(int life) {
    this.life = life;
  }

  public void increment(int l) {
    this.life += l;
  }

}
