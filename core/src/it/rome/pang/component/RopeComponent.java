package it.rome.pang.component;

import com.artemis.Component;
import com.badlogic.gdx.graphics.Texture;


/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class RopeComponent extends Component {

  float speed;
  float height;
  Texture texture;

  public RopeComponent(float height, float speed, Texture texture) {
    this.height = height;
    this.speed = speed;
    this.texture = texture;
  }

  public float getHeight() {
    return height;
  }

  public Texture getTexture() {
    return texture;
  }

  public float getSpeed() {
    return speed;
  }

}
